#! /usr/bin/bash

# Deploy OpenWhisk on a Google Kubernetes Cluster.

### CONFIGURATION ###
SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
ROOTDIR="$(realpath "$SCRIPTDIR/../..")"
BINDIR="$ROOTDIR/bin"

CLI="$BINDIR/wsk"

# Name of the Google Kubernetes Cluster.
GKE_CLUSTER="ow-cluster"

# Helm release of the deployment of the Nginx ingress.
INGRESS_REL="ow-ingress-nginx"
# Helm release of the deployment of OpenWhisk.
OW_REL="ow"
# Namespace of Kubernetes cluster where OpenWhisk.
NS="openwhisk"

# Number of nodes in the GKE cluster.
NUM_NODES=2
# Machine type of the GKE cluster.
# To match the minimum recommended, use at least e2-medium.
MACHINE_TYPE="e2-medium"
# Size of the disks (GiB) of the nodes in the GKE cluster.
DISK_SIZE="32"

AUTH="23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP"
### CONFIGURATION ###


### DISPLAY ROUTINES
if tput colors >/dev/null 2>&1; then
    reset="\e[0m"

    normal="\e[22m"
    bold="\e[1m"

    red="\e[0;91m"
    yellow="\e[0;93m"
    blue="\e[0;94m"
fi

echo_info() {
    echo -e "$blue$*$reset"
}

echo_warn() {
    echo -e "$yellow$*$reset"
}

echo_err() {
    echo -e "$red$*$reset" 1>&2
}

die() {
    echo_err "$*"
    exit 1
}
### DISPLAY ROUTINES

### DEPENDENCY CHECKS ###
if ! which gcloud > /dev/null; then
    echo_err "missing dependency: gcloud"
    echo_err "see: https://cloud.google.com/sdk/docs/install"
    exit 3
fi

if ! which helm > /dev/null; then
    echo_err "missing dependency: helm"
    echo_err "see: https://helm.sh/docs/intro/install/"
    exit 3
fi

if ! which kubectl > /dev/null; then
    echo_err "missing dependency: kubectl"
    echo_err "see: https://helm.sh/docs/intro/install/"
    exit 3
fi

if [ "$(gcloud components list --filter gke-gcloud-auth-plugin --format 'value(state.name)' 2> /dev/null)" != "Installed" ]; then
    echo_err "missing dependency: gke-cloud-auth-plugin"
    echo_err "see: https://cloud.google.com/kubernetes-engine/docs/how-to/\
cluster-access-for-kubectl#install_plugin"
    exit 3
fi

if [ "$(gcloud services list --enabled --filter "container.googleapis.com" --format 'value(state)' 2> /dev/null)" != "ENABLED" ]; then
    echo_info "enabling Kubernetes Engine API on Google Cloud"

    gcloud services enable container.googleapis.com > /dev/null ||
        die "failed enabling Kubernetes Engine API \"container.googleapis.com\""
fi
### DEPENDENCY CHECKS ###

# Create the GKE cluster.
if ! gcloud container clusters describe "$GKE_CLUSTER" >/dev/null 2>&1; then
    echo_info "creating Google Kubernetes Engine cluster $GKE_CLUSTER"
    echo_warn "this can be a very long operation (> 12min)"

    gcloud container clusters create "$GKE_CLUSTER" \
        --num-nodes "$NUM_NODES" --machine-type "$MACHINE_TYPE" \
        --disk-type "pd-balanced" --disk-size "$DISK_SIZE" \
        --location-policy=BALANCED \
        --enable-ip-alias ||
        die "failed creating GKE cluster $GKE_CLUSTER"
else
    echo_warn "using existing GKE cluster $bold$GKE_CLUSTER$normal"
fi

echo_info "getting credentials for the GKE cluster $GKE_CLUSTER"

gcloud container clusters get-credentials "$GKE_CLUSTER" ||
    die "failed getting credentials for the GKU cluster $GKE_CLUSTER"

# Add the Helm repository for the Nginx ingress.
if ! helm repo list --output json |\
    jq -r '.[].name' |\
    grep --quiet '^ingress-nginx$'
then
    echo_info "adding Helm repository: ingress-nginx"

    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx ||
        die "failed adding Helm repository for ingress-nginx"
else
    echo_warn "using already present Helm repository ingress-nginx"
fi

helm repo update >/dev/null

# Install the Helm chart of the Nginx ingress.
if ! helm --namespace "$NS" status "$INGRESS_REL" >/dev/null 2>&1; then
    echo_info "installing Helm chart of Nginx ingress in namespace $bold$NS$normal under release $bold$INGRESS_REL$normal"

    helm install --namespace "$NS" --create-namespace "$INGRESS_REL" \
        ingress-nginx/ingress-nginx ||
        die "failed installing Nginx ingress; recover with $bold\`helm uninstall --namespace $NS $INGRESS_REL\`$normal"
else
    echo_warn "using existing Helm release $bold$INGRESS_REL$normal in namespace $bold$NS$normal for ingress"
fi

ingress_ip=$(kubectl --namespace "$NS" get services "$INGRESS_REL-controller" \
    --output jsonpath='{.status.loadBalancer.ingress[0].ip}' 2>/dev/null);
[ $? -ne 0 ] && die "failed getting ingress IP address"

if [ -z "$ingress_ip" ]; then
    echo_info "waiting for ingress deployment"

    while [ -z "$ingress_ip" ]; do
        ingress_ip=$(kubectl --namespace "$NS" get services "$INGRESS_REL-controller" \
            --output jsonpath='{.status.loadBalancer.ingress[0].ip}' 2>/dev/null);

        sleep 2
    done
fi

echo_info "public IP address of the OpenWhisk deployment: $bold$ingress_ip$normal"

# nip.io is a public service that resolves domain name queries by answering the
# IP address embedded in the domain name.
# As noted on its webpage: "This is a free service provided by Exentrique
# Solutions (the same people who run XP-Dev.com which offer Git, Mercurial and
# Subversion hosting)."
api_hostname="$ingress_ip.nip.io"
api_hostport=443

# Label cluster nodes as invokers for OpenWhisk.
nodes=$(kubectl get nodes --output jsonpath='{.items[*].metadata.name}') ||
    die "failed getting node list of the cluster"

for node in $nodes; do
    if [ -z "$(kubectl get nodes --output jsonpath='{.metadata.labels.openwhisk\-role}' "$node")" ]; then
        echo_info "labeling cluster node $node with the invoker role for OpenWhisk"

        kubectl label node "$node" "openwhisk-role=invoker" ||
            die "failed labeling node $node with the invoker role"
    else
        echo_warn "using already labeled node $node"
    fi
done

# Install the Helm chart of OpenWhisk.
if ! helm --namespace "$NS" status "$OW_REL" >/dev/null 2>&1; then
    echo_info "making mycluster.yaml deployment file"

    "$SCRIPTDIR"/make_mycluster.sh "$api_hostname" ||
        die "failed making mycluster.yaml"

    echo_info "installing Helm chart of OpenWhisk in namespace $bold$NS$normal under release $bold$OW_REL$normal"

    helm install "$OW_REL" "$ROOTDIR/helm/openwhisk" \
        --namespace "$NS"\
        --values "$SCRIPTDIR/mycluster.yaml" ||
        die "failed deploying OpenWhisk with Helm"
else
    echo_warn "using existing Helm release $bold$OW_REL$normal in namespace $bold$NS$normal for OpenWhisk"
fi

pod_status="$(kubectl --namespace "$NS" get pods\
    --selector 'name='"$OW_REL"'-install-packages'\
    --output jsonpath='{.items[0].status.phase}')"

if [ "$pod_status" != Succeeded ]; then
    echo_info "waiting for OpenWhisk to deploy"
    echo_warn "the script will poll many times to check the deployment status"

    try_n=0
    while [ "$pod_status" != Succeeded ]; do
        sleep 10

        try_n=$((try_n + 1))

        echo_info "testing status of $OW_REL-install-packages ($try_n)"

        # In addition to waiting for the install-packages pod, check that other
        # core pods have not failed.
        # Check install-packages last to keep its status in pod_status.
        for pod in couchdb kafka controller invoker install-packages; do
            pod_status="$(kubectl --namespace "$NS" get pods\
                --selector 'name='"$OW_REL-$pod"\
                --output jsonpath='{.items[0].status.phase}')"
            if [ "$pod_status" = Failed ]; then
                die "failed deploying OpenWhisk: pod $pod failed"
            fi
        done
    done
else
    echo_warn "using already deployed OpenWhisk"
fi

echo_info "API endpoint: $bold$api_hostname:$api_hostport$normal"

# Set up OpenWhisk CLI.
if [ ! -x "$CLI" ]; then
    echo_info "downloading OpenWhisk CLI to $bold\"$CLI\"$normal"

    mkdir -p "$BINDIR"
    curl -L "$CLI_DOWNLOAD_URL" | tar -C "$BINDIR" -xz wsk ||
        die "failed downloading wsk CLI"
else
    echo_warn "using existing OpenWhisk CLI program at $bold\"$CLI\"$normal"
fi

if [ "$("$CLI" property get --output raw --apihost)" != "$APIHOST" ]; then
    echo_warn "configure API host in OpenWhisk CLI:"
    echo_warn "\t$bold\`$CLI property set --apihost \"$api_hostname:$api_hostport\"\`$normal"
else
    echo_warn "API host in OpenWhisk CLI already configured to use the kind\
        cluster at $bold\"$api_hostname:$api_hostport\"$normal"
fi

if [ "$("$CLI" property get --output raw --auth)" != "$AUTH" ]; then
    echo_warn "configure auth in OpenWhisk CLI:"
    echo_warn "\t$bold\`$CLI property set --auth \"$AUTH\"\`$normal"
else
    echo_warn "Auth in OpenWhisk CLI already configured for the kind cluster"
fi

echo_info "deployment done"
echo

