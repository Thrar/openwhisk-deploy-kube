#! /usr/bin/bash

# Make the mycluster.yaml file that described the cluster to deploy OpenWhisk.

### CONFIGURATION ###
SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"

# Certificate of the OpenWhisk deployment.
CRT_FILE="$SCRIPTDIR/cert.pem"
# Encryption key of the certificate.
KEY_FILE="$SCRIPTDIR/privkey.pem"
### CONFIGURATION ###

### DISPLAY ROUTINES
if tput colors >/dev/null 2>&1; then
    reset="\e[0m"

    normal="\e[22m"
    bold="\e[1m"

    red="\e[0;91m"
    yellow="\e[0;93m"
    blue="\e[0;94m"
fi

echo_info() {
    echo -e "$blue$*$reset"
}

echo_warn() {
    echo -e "$yellow$*$reset"
}

echo_err() {
    echo -e "$red$*$reset" 1>&2
}

die() {
    echo_err "$*"
    exit 1
}
### DISPLAY ROUTINES

if [ $# -lt 1 ]; then
    echo_err "missing argument"
    echo
    echo "Usage: $0 DOMAIN"
    echo
    echo "* DOMAIN: domain name of the OpenWhisk cluster"
    echo
    echo "A DOMAIN can be constructed via the service nip.io from the IP address"
    echo "of the cluster: simply append \".nip.io\" to the external IP address."
    echo
    echo "The script will run openssl to generate a pair of certificate and key."
    echo "They are embedded as base64 in the file mycluster.yaml."
    exit 2
fi >&2

domain="$1"

if ! which openssl >/dev/null 2>&1; then
    echo_err "missing dependency: helm."
    exit 3
fi

echo_info "making self-signed certificate $bold\"$CRT_FILE\"$normal with key $bold\"$KEY_FILE\"$normal"

[ -f "$CRT_FILE" ] || [ -f "$KEY_FILE" ] && die "certificate file $bold\"$CRT_FILE\"$normal or key file $bold\"$KEY_FILE\"$normal already exists"

openssl req \
    -subj "/C=FR/CN=$domain/O=$domain" \
    -addext "subjectAltName = DNS:$domain" \
    -x509 -noenc -days 365 -newkey rsa:2048 -keyout "$KEY_FILE" -out "$CRT_FILE" \
    >/dev/null 2>&1 ||
die "failed making signed certificate"

echo_info "making mycluster.yaml to deploy OpenWhisk at domain $bold$domain$normal"

cat > "$SCRIPTDIR/mycluster.yaml" <<EOF
whisk:
  ingress:
    apiHostName: "$domain"
    apiHostPort: 443
    apiHostProto: https
    type: Standard
    domain: "$domain"
    tls:
      enabled: true
      secretenabled: true
      createsecret: true
      secretname: openwhisk-ingress-tls-secret
      secrettype: kubernetes.io/tls
      crt: >-
        $(base64 -w0 "$CRT_FILE")
      key: >-
        $(base64 -w0 "$KEY_FILE")
    annotations:
      kubernetes.io/ingress.class: nginx
      kubernetes.io/tls-acme: true
      nginx.ingress.kubernetes.io/proxy-body-size: 0
EOF

