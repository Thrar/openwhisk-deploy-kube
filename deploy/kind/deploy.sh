#! /usr/bin/bash

# Deploy OpenWhisk to a local kind Kubernetes cluster.

# CONFIGURATION {{{
SCRIPTDIR="$(cd "$(dirname "$0")" && pwd)"
ROOTDIR="$(realpath "$SCRIPTDIR/../..")"
BINDIR="$ROOTDIR/bin"

CLI="$BINDIR/wsk"

# Name of the kind cluster.
CLUSTER="openwhisk"
# Helm release of the deployment of OpenWhisk.
REL="ow"
# Namespace of the deployment of OpenWhisk in the Kubernetes cluster.
NS="openwhisk"

CLI_DOWNLOAD_URL="https://github.com/apache/openwhisk-cli/releases/download/1.2.0/OpenWhisk_CLI-1.2.0-linux-amd64.tgz"

APIHOST="localhost:31001"
AUTH="23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP"
# CONFIGURATION }}}

# DISPLAY ROUTINES {{{
if tput colors >/dev/null 2>&1; then
    reset="$(tput sgr0)"

    bold="$(tput bold)"
    # tput does not have a default capability to turn bold off, but most often
    # it is 22.
    normal="\e[22m"
    underline="$(tput smul)"
    nounderline="$(tput rmul)"

    red="$(tput setaf 1)"
    green="$(tput setaf 2)"
    yellow="$(tput setaf 3)"
    blue="$(tput setaf 4)"
fi

echo_info() {
    echo -e "$blue$*$reset"
}

echo_warn() {
    echo -e "$yellow$*$reset"
}

echo_err() {
    echo -e "$red$*$reset" 1>&2
}

die() {
    echo_err "$*"
    exit 1
}
# DISPLAY ROUTINES }}}

# OTHER ROUTINES {{{
quit_or_continue() {
    if [ "$stop_after" = "$1" ]; then
        shift
        echo_info "stopping after $*"
        exit 0;
    fi
}
# OTHER ROUTINES }}}

# DEPENDENCY CHECKS {{{
if ! which kind > /dev/null; then
    echo_err "missing dependency: kind"
    echo_err "see: https://kind.sigs.k8s.io/docs/user/quick-start/#installation"
    exit 3
fi

if ! which helm > /dev/null; then
    echo_err "missing dependency: helm"
    echo_err "see: https://helm.sh/docs/intro/install/"
    exit 3
fi

if ! which kubectl >/dev/null; then
    echo_err "missing dependency: kubectl"
    echo_err "see: https://kubernetes.io/docs/tasks/tools/#kubectl"
    exit 3
fi
# DEPENDENCY CHECKS }}}

# ARGUMENTS {{{
# See usage message.
kind_cluster_config_file="$SCRIPTDIR/kind-cluster.yaml"
helm_mycluster_addon_file=""
stop_after=""

echo_usage() {
    echo -e "Usage: $bold$red$0$reset [--kind-config FILE] [--helm-addon-values FILE] [--stop-after STEP]"
    echo
    echo -e "Deploy OpenWhisk to a local kind Kubernetes cluster."
    echo
    echo -e "Options (only long forms):"
    echo
    echo -e "\t$bold$red--kind-config ${green}FILE$reset"
    echo -e "\t\tSet $bold${green}FILE$reset as the configuration file to deploy the kind cluster."
    echo -e "\t\tNote: kind can only take one configuration file, so $bold${green}FILE$reset ${underline}replaces$nounderline the default one at $bold\"$kind_cluster_config_file\"$normal."
    echo
    echo -e "\t$bold$red--helm-addon-values ${green}FILE$reset"
    echo -e "\t\tAdd $bold${green}FILE$reset as a file of chart values to deploy the Helm chart of OpenWhisk."
    echo -e "\t\tIn any case, the default file $bold\"$SCRIPTDIR/mycluster.yaml\"$normal is used;"
    echo -e "\t\tif specified, $bold${green}FILE$reset is ${underline}added$nounderline after the default file (so it will override its values)."
    echo
    echo -e "\t$bold$red--stop-after ${green}STEP$reset"
    echo -e "\t\tStop the deployment after $bold${green}STEP$reset."
    echo -e "\t\tRunning $bold$0$reset again will pick up where it left."
    echo
    echo -e "\t\tSteps:"
    echo -e "\t\t\t$bold${green}init$reset: check dependencies and parse arguments"
    echo -e "\t\t\t$bold${green}kind-cluster$reset: create the kind cluster"
    echo -e "\t\t\t$bold${green}helm-chart$reset: install the Helm chart"
    echo -e "\t\t\t$bold${green}cli$reset: download the OpenWhisk CLI"
    echo
    echo -e "\t$bold$red--help$reset"
    echo -e "\t\tDisplay this help text."
}

check_param_value() {
    if [ -z "$2" ]; then
        echo_err "missing value for option \"$1\""
        echo_usage
        exit 2
    fi
}

while [ $# -gt 0 ]; do
    case "${1#--}" in
        "kind-config")
            check_param_value "$1" "$2"
            kind_cluster_config_file="$2"
            shift 2

            echo_info "using kind cluster configuration file $bold\"$kind_cluster_config_file\"$normal"
            ;;
        "helm-addon-values")
            check_param_value "$1" "$2"
            helm_mycluster_addon_file="$2"
            shift 2

            echo_info "using additional Helm chart values file $bold\"$helm_mycluster_addon_file\"$normal"
            ;;
        "stop-after")
            check_param_value "$1" "$2"
            stop_after="$2"
            shift 2

            case "$stop_after" in
                "init"|"kind-cluster"|"helm-chart"|"cli")
                    echo_info "the script will stop after step $bold$stop_after$normal";;
                *)
                    echo_err "unknown step to stop at: $bold$stop_after$reset"
                    echo
                    echo_usage
                    exit 2
            esac
            ;;
        "help")
            echo_usage
            exit 0
            ;;
        *)
            echo_err "unknown option \"$1\""
            echo
            echo_usage
            exit 2
    esac
    # Bash does not care if there are not enough remaining arguments to shift.
done
# ARGUMENTS }}}

quit_or_continue "init" "initializing"

if ! kind get clusters | grep --quiet "^$CLUSTER$"; then
    echo_info "starting kind cluster $bold$CLUSTER$normal"

    "$ROOTDIR/deploy/kind/start-kind.sh" "$CLUSTER" "$kind_cluster_config_file" ||
        die "failed starting kind with OpenWhisk's configuration"
else
    echo_warn "using existing kind cluster $bold$CLUSTER$normal"
fi

quit_or_continue "kind-cluster" "starting the kind cluster"

# Install the Helm chart of OpenWhisk.
if ! helm --namespace "$NS" status "$REL" >/dev/null 2>&1; then
    echo_info "installing Helm chart of OpenWhisk in namespace $bold$NS$normal under release $bold$REL$normal"

    if [ -n "$helm_mycluster_addon_file" ]; then
        addon_values="$(printf -- "--values %q" "$helm_mycluster_addon_file")"
    fi

    helm install "$REL" "$ROOTDIR/helm/openwhisk"\
        --namespace "$NS" --create-namespace\
        --values "$SCRIPTDIR/mycluster.yaml" $addon_values\
        --wait --timeout 10m0s ||
        die "failed deploying OpenWhisk with Helm"
else
    echo_warn "using existing Helm release $bold$REL$normal in namespace $bold$NS$normal for OpenWhisk"
fi

quit_or_continue "helm-chart" "installing Helm chart"

# Set up OpenWhisk CLI.
if [ ! -x "$CLI" ]; then
    echo_info "downloading OpenWhisk CLI to $bold\"$CLI\"$normal"

    mkdir -p "$BINDIR"
    curl -L "$CLI_DOWNLOAD_URL" | tar -C "$BINDIR" -xz wsk ||
        die "failed downloading wsk CLI"
else
    echo_warn "using existing OpenWhisk CLI program at $bold\"$CLI\"$normal"
fi

quit_or_continue "cli" "downloading OpenWhisk CLI"

if [ "$("$CLI" property get --output raw --apihost)" != "$APIHOST" ]; then
    echo_warn "USER ACTION REQUIRED: configure API host in OpenWhisk CLI:"
    echo_warn "\t$bold\`$CLI property set --apihost \"$APIHOST\"\`$normal"
else
    echo_warn "API host in OpenWhisk CLI already configured to use the kind cluster at $bold\"$APIHOST\"$normal"
fi

if [ "$("$CLI" property get --output raw --auth)" != "$AUTH" ]; then
    echo_warn "USER ACTION REQUIRED: configure auth in OpenWhisk CLI:"
    echo_warn "\t$bold\`$CLI property set --auth \"$AUTH\"\`$normal"
else
    echo_warn "auth in OpenWhisk CLI already configured for the kind cluster"
fi

echo_info "done"

