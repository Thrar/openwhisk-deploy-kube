# Enabling FaaSIO

## Overview

Just enabling FaaSIO during the deployment of OpenWhisk will not use FaaSIO for data passing between functions in a chain: OpenWhisk's FaaSIO fork must be deployed.

For more information about FaaSIO, see [OpenWhisk's FaaSIO fork](https://gitlab.com/Thrar/openwhisk/-/tree/faasio).

## Requirements

FaaSIO is currently only compatible with Kubernetes deployments with a single invoker node (e.g., [a kind cluster](docs/k8s-kind.md) with only one worker).
Kubernetes must be able to provision a volume of type `hostPath` on this node.

## Enabling FaaSIO

Customize your mycluster.yaml, or write another file named faasio.yaml, to include the following new values for the deployment with Helm:

```yaml
faasio:
  enable: true
controller:
  lean: true
```

For now, FaaSIO requires the lean controller to make sure the pipes are created in all function containers.
By deploying "lean", it ensures that there are no invokers, so running a function necessarily goes through the controller's code that creates containers.

## Configuring FaaSIO

Further settings can be changed related to FaaSIO:

* `volume`:
  * `name`: name of the volume in the invoker and the functions' pods
  * `hostPath`: path **on the host** that is mounted in the invoker and the functions's pods through the volume
  * `mountPath`: path **in the pods** where the volume is mounted

For instance (example of the default values in the Helm chart, save `enable` that is `false`):

```yaml
faasio:
  enable: true
  volume:
    name: faasio
    hostPath: "/var/run/faasio"
    mountPath: "/var/run/faasio"
```
